import { TableRow, TableCell, TableContainer, TableHead, TableBody, Paper, Table } from "@mui/material"

export default function ExpenseTable({expense, sum}) {
      

    return (
        <TableContainer component={Paper} className='informationTable' sx={{ width: 850, background:'#A188A9', }}>
          <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Cost</TableCell>
              <TableCell>Category</TableCell>
            </TableRow>
            
          </TableHead>
          <TableBody>
            {expense.map((entry, i) => (                       
              <TableRow key={i} >                                                         
                <TableCell>{entry.name}</TableCell>
                <TableCell>{entry.cost}</TableCell>
                <TableCell>{entry.category}</TableCell>            
              </TableRow>
            ))}     
               <TableRow>
               <TableCell>Total:</TableCell>
               <TableCell>{sum}</TableCell>
               <TableCell></TableCell>
               </TableRow>
          </TableBody>
          </Table>        
        </TableContainer>
      )
}