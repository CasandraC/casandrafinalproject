import React, { useEffect, useState} from "react"
import {Container, Paper, Grid, TextField, Select, MenuItem, Button} from "@mui/material"

const emptyValues = {
  name: "",
  cost : 0,
  category: "",
}

export default function ExpenseTrackerForm({ addEntryToExpenses, sum }) {
    const [newEntry, setNewEntry] = useState(emptyValues)
    const [budget, setBudget] = useState(0)
    const handleChange = (event) => {
      setBudget(event.target.value);
    }    

    const onChange = e => {
      setNewEntry(prevValues => ({ ...prevValues, [e.target.name]: e.target.value }))
    }

    const budgetRemaining = (Number(budget)-Number(sum))

    const color = budgetRemaining >= 0 ? '#E2CDEE' : '#BD2D2B'

    const message = color === '#E2CDEE' ? 'This is your budget remaining' : 'You exceded your budget'

  useEffect(() => {
    document.getElementById("budgetRem").style.background = color
  }, [color])

  return (
      <form >
        <Container> 
        <h1 className="header"> EXPENSE TRACKER </h1>
        <h2 className="App-link"> August </h2>
        <div>       
          <div> 
          <Grid container spacing={2}>
          <Grid item >
            <Paper elevation={0} style={{height:130, width:500,background:'#E2CDEE', }}>              
                  <Paper elevation={0} style={{padding:1,background:'#E2CDEE', }}><p style={{color:"#510E1E",}}>Enter your Budget</p></Paper>
                  <Paper elevation={0} style={{background:'transparent', }}>
                  <TextField
                        id="outlined-multiline-flexible"                                            
                        multiline                     
                        value={budget}
                        onChange={handleChange}> 
                  </TextField>
                  </Paper>                                  
                                  
                          
            </Paper>
            </Grid> 
            <Grid item>
                <Paper style={{height:100, width:1030,background:'#E2CDEE',}}>
                    <Paper elevation={0} style={{padding:1,background:'#E2CDEE', }}><h1 name="budget" >${budget}</h1><p style={{color:"#510E1E",}}>Initial budget</p></Paper>                                                                                                                    
                </Paper>
            </Grid> 
            <Grid item>
                <Paper id="budgetRem" style={{height:100, width:510,background:'#E2CDEE',}}><h1>${budgetRemaining}</h1>
                <label>{message}</label>
                </Paper>
            </Grid>   
            <Grid item>
                <Paper style={{height:100, width:510,background:'#E2CDEE'}}><h1>${sum}</h1>
                <label>Total spent</label>
                </Paper>
            </Grid>  
            <Grid item>
                  <Paper style={{height:250, width:1030,background:'#D8A9F3'}}>                
                  <h1 className="expense-header">Add a New Expense</h1>                 
                  <TextField name='name'type='text'placeholder="Expense name" 
                          value={newEntry.name}
                          onChange={onChange}>     
                  </TextField>
                    <TextField name='cost'type='text' placeholder="Cost"    
                          value={newEntry.cost}                       
                          onChange={onChange}                           
                           >                                                  
                    </TextField>
                    <Select style={{width:300}} labelId="demo-simple-select-label"
                            label="category"
                            id="demo-simple-select"
                            name='category'                          
                            value={newEntry.category}
                            onChange={onChange}>
                          <MenuItem value="Food">Food</MenuItem>
                          <MenuItem value="Clothing">Clothing</MenuItem>
                          <MenuItem value="Healthcare">Healthcare</MenuItem>
                          <MenuItem value="School">School</MenuItem>
                          <MenuItem value="Sports">Sports</MenuItem>
                          <MenuItem value="Bills">Bills</MenuItem>
                          <MenuItem value="Car">Car</MenuItem>                                              
                    </Select>  
                    <div className="addButton">
                      <Grid item>
                      <Button style={{ padding: "20px", height:25, width:100,background:'#80BECC'}} 
                          variant='contained' onClick={() => {
                          addEntryToExpenses(newEntry)
                          setNewEntry(emptyValues)                                          
                        }}       
                        >ADD                                  
                      </Button>
                      </Grid>
                    </div>  
                    </Paper>                                                   
            </Grid> 
          </Grid>          
          </div>
         </div> 
         </Container>  
      </form>
    );
  
}