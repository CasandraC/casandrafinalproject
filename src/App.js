/* eslint-disable no-unused-vars */
import "./App.css"
import "@fontsource/roboto/300.css"
import "@fontsource/roboto/400.css"
import "@fontsource/roboto/500.css"
import "@fontsource/roboto/700.css"


import Composition from "./pages/ExpenseTrackerApp"

const App = () => {
  return (
    <div className='App'>
      <Composition />
    </div>
  )
}

export default App
