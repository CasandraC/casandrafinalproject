import React, {useState} from 'react';
import ExpenseTrackerForm from "../components/ExpenseTrackerForm"
import ExpensesTable from "../components/ExpensesTable"

export default function ExpenseTrackerApp() {

  const [expense, setExpense] = useState([])
  const [sum, setSum] = useState(0);


  return (
    <>
      <ExpenseTrackerForm sum ={sum}
        addEntryToExpenses={newEntry => {
          if (Object.values(newEntry).every(v => v)) {
            setExpense(prevValues => [...prevValues, newEntry])           
            setSum(Number(sum)+Number(newEntry.cost))
          }
        }}        
      />
      <ExpensesTable expense={expense} sum={sum}/>      
    </>
  )
}
